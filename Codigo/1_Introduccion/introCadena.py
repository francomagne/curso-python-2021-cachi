# https://www.pitt.edu/~naraehan/python2/string_methods1.html
# https://www.programiz.com/python-programming/methods/string

x = raw_input("ingrese cadena: ")

if x!="":
	y = ""
	for element in x:
		y = element + y      

	print " la cadena " + str(x)
	print " la cadena invertida " + str(y)
	
	print " upper " + str(x.upper())
	print " lower " + str(x.lower())
  
	if x.isdigit():
		print "son todos digitos " + str(x)
	else:
		print "algunos caracteres no son digitos " + str(x)

	
	subC = raw_input("ingrese subcadena a buscar: ")
	if subC!="":
		print "la subC esta en pos " + str(x.index(subC))
		#str.index(sub[, start[, end]] )
	else:
		print "subcadena vacia. Reintente"
		
	
	lista = x.split(" ")
	# formo una lista con las palabras de la cadena
	#str.split([separator [, maxsplit]])
	
	print str(lista)
	

else:
	print " cadena vacia. Reintente"
