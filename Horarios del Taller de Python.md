<center>
<img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQo80FbjBz3iyqBdNgQT0qaWbjMviYo2MQL0VjnVnPGsY1U9twWHOZAG0nGI3y9gYSrCWo&usqp=CAU">

# Taller de Programación en Python

| Horario | Link de Sala Meet |
| ---------- | ---------- |
| Jueves de 15 a 18hs | https://meet.google.com/rrr-hhnw-hps |

### Responsable
| Dr. Cristian Martínez |
| ---------- |
| cmartinez@di.unsa.edu.ar |
| <img src="img/cm.jpg" width="150px" height="60%">|

### Equipo
| Lic. Ismael Orozco  | Mag. José I. Tuero |
| ---------- | ---------- |
| ciorozco.unsa@gmail.com | <center>jituero@gmail.com |
| <center><img src="img/io.jpg" width="150px" height="20%"> | <img src="img/jt.jpg" width="140px" height="28%"> |

| Franco Alanís Magne | Nicolas Morales Juarez |
| ---------- | ---------- |
| francomagne96@gmail.com| ren.nicolas92@gmail.com |
| <center><img src="img/fm.jpg" width="140px" height="20%"> | <img src="img/nm.jpg" width="140px" height="20%"> |

### Organizadores
<img src="img/di_logo.png" width="130px" height="20%"> <img src="img/IED.jpg" width="120px" height="20%">
